#pragma once
#include <string>
#include "Inventar.h"
class Eigenschaften
{
private:
	std::string name_;
	std::string beschreibung_ = "";
	Inventar inventar_;


public:
	Eigenschaften(std::string name, std::string beschreibung, Inventar inventar);
	~Eigenschaften();

	std::string getName() const;
	std::string getBeschreibung() const;

	void setName(std::string name);
	void setBeschreibung(std::string beschreibung);
};

