#pragma once
#include <vector>
#include "Raum.h"
class RaumFabrik
{
private:
	std::vector<Raum> raeume_;

	Raum* r1;
	Raum* r2;
	Raum* r3;

void setzeVerbindungen();
public:
	RaumFabrik();
	~RaumFabrik();
	std::vector<Raum> getRaeume() const;
};

