#include "RaumFabrik.h"

RaumFabrik::RaumFabrik()
{
	raeume_.push_back(*(new Raum("Raum 1", "Das ist die Beschreibung des ersten Raumes", Inventar())));
	raeume_.push_back(*(new Raum("Raum 2", "Das ist die Beschreibung des zweiten Raumes", Inventar())));
	raeume_.push_back(*(new Raum("Raum 3", "Das ist die Beschreibung des dritten Raumes", Inventar())));
	raeume_.push_back(*(new Raum("Raum 4", "Das ist die Beschreibung des virten Raumes", Inventar())));
	raeume_.push_back(*(new Raum("Raum 5", "Das ist die Beschreibung des fuenften Raumes", Inventar())));

	setzeVerbindungen();
}

RaumFabrik::~RaumFabrik()
{
}

std::vector<Raum> RaumFabrik::getRaeume() const
{
	return raeume_;
}

void RaumFabrik::setzeVerbindungen()
{
	raeume_[0].setROsten(&raeume_[1]);

	raeume_[1].setRWesten(&raeume_[0]);
	raeume_[1].setROsten(&raeume_[2]);

	raeume_[2].setRWesten(&raeume_[1]);
	raeume_[2].setRNorden(&raeume_[3]);
	raeume_[2].setRSueden(&raeume_[4]);


	raeume_[3].setRSueden(&raeume_[2]);

	raeume_[4].setRNorden(&raeume_[2]);
}
