#pragma once
#include <memory>
#include "Inventar.h"
#include <string>
#include "Eigenschaften.h"
enum class richtung {
	norden, sueden, westen, osten
};

class Raum : public Eigenschaften
{
private:
	Raum *rNorden_p_;
	Raum *rOsten_p_;
	Raum *rSueden_p_;
	Raum *rWesten_p_;


public:
	~Raum();
	Raum(Raum* rNorden_p, Raum* rOsten_p, Raum* rSueden_p, Raum* rWesten_p, std::string name, std::string beschreibung, Inventar inventar);
	Raum(std::string name, std::string beschreibung, Inventar inventar);

	Raum* getRNorden() const;
	Raum* getROsten() const;
	Raum* getRSueden() const;
	Raum* getRWesten() const;

	void setRNorden(Raum* rNorden);
	void setROsten(Raum* rOsten);
	void setRSueden(Raum* rSueden);
	void setRWesten(Raum* rWesten);

	Raum* gehen(richtung r);
};

