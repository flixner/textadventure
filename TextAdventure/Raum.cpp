#include "Raum.h"
#include <iostream>

Raum::~Raum()
{
}

Raum::Raum(Raum* rNorden_p, Raum* rOsten_p, Raum* rSueden_p, Raum* rWesten_p, std::string name, std::string beschreibung, Inventar inventar)
	:Eigenschaften(name, beschreibung, inventar)
{
	rNorden_p_ = rNorden_p;
	rOsten_p_ = rOsten_p;
	rSueden_p_ = rSueden_p;
	rWesten_p_ = rWesten_p;
}

Raum::Raum(std::string name, std::string beschreibung, Inventar inventar)
	:Eigenschaften(name, beschreibung, inventar)
{
	rNorden_p_ = nullptr;
	rOsten_p_ = nullptr;
	rSueden_p_ = nullptr;
	rWesten_p_ = nullptr;
}

Raum* Raum::getRNorden() const
{
	return rNorden_p_;
}

Raum* Raum::getROsten() const
{
	return rOsten_p_;
}

Raum* Raum::getRSueden() const
{
	return rSueden_p_;
}

Raum* Raum::getRWesten() const
{
	return rWesten_p_;
}

void Raum::setRNorden(Raum* rNorden)
{
	rNorden_p_ = rNorden;
}

void Raum::setROsten(Raum* rOsten)
{
	rOsten_p_ = rOsten;
}

void Raum::setRSueden(Raum* rSueden)
{
	rSueden_p_ = rSueden;
}

void Raum::setRWesten(Raum* rWesten)
{
	rWesten_p_ = rWesten;
}

Raum* Raum::gehen(richtung r)
{
	switch (r)
	{
	case richtung::norden:
		if (rNorden_p_ != nullptr)
		{
			return rNorden_p_;
		}
		else
		{
			std::cerr << "Raum nicht vorhanden (Andere Richtung ausw�hlen)";
		}
		break;
	case richtung::sueden:
		if (rSueden_p_ != nullptr)
		{
			return rSueden_p_;
		}
		else
		{
			std::cerr << "Raum nicht vorhanden (Andere Richtung ausw�hlen)";
		}
		break;
	case richtung::westen:
		if (rWesten_p_ != nullptr)
		{
			return rWesten_p_;
		}
		else
		{
			std::cerr << "Raum nicht vorhanden (Andere Richtung ausw�hlen)";
		}
		break;
	case richtung::osten:
		if (rOsten_p_ != nullptr)
		{
			return rOsten_p_;
		}
		else
		{
			std::cerr << "Raum nicht vorhanden (Andere Richtung ausw�hlen)";
		}
		break;
	default:
		break;
	}
}
