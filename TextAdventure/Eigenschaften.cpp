#include "Eigenschaften.h"



Eigenschaften::Eigenschaften(std::string name, std::string beschreibung, Inventar inventar)
	:name_(name), beschreibung_(beschreibung)
{
	inventar_ = inventar;
}

Eigenschaften::~Eigenschaften()
{
}

std::string Eigenschaften::getName() const
{
	return name_;
}

std::string Eigenschaften::getBeschreibung() const
{
	return beschreibung_;
}

void Eigenschaften::setName(std::string name)
{
	name_ = name;
}

void Eigenschaften::setBeschreibung(std::string beschreibung)
{
	beschreibung_ = beschreibung;
}
